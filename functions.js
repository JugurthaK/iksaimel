function getJoueurs() {
  let url = "./functions.php?idf=1";
  ajaxGet(url, function(reponse) {
    if (reponse == 404) {
      console.log("Il y a un souci");
    } else {
      let joueurs = JSON.parse(reponse);
      joueurs.forEach(joueur => {
        let identificateur = "pointsPositifsfJoueur" + joueur.id;
        document.getElementById(identificateur).innerHTML = joueur.points;
        let idArgent = "argentJoueur" + joueur.id;
        document.getElementById(idArgent).innerHTML = joueur.argent;
        let idEpargne = "epargneJoueur" + joueur.id;
        document.getElementById(idEpargne).innerHTML = joueur.epargne;
      });
    }
  });
}
function getJoueur(numJoueur) {
  let url = "./functions.php?idf=2&id=" + numJoueur;
  ajaxGet(url, function(reponse) {
    if (reponse == 404) {
      console.log("Il y a un souci");
    } else {
      return JSON.parse(reponse);
    }
  });
}

function addPoint(numJoueur) {
  let url = "./functions.php?idf=3&id=" + numJoueur;
  ajaxGet(url, function(reponse) {
    if (reponse == 404) {
      console.log("Il y a un souci");
    } else {
      let joueur = JSON.parse(reponse);
      let identificateur = "pointsPositifsfJoueur" + joueur.id;
      document.getElementById(identificateur).innerHTML = joueur.points;
      let idenArgent = "argentJoueur" + joueur.id;
      document.getElementById(idenArgent).innerHTML = joueur.argent;
    }
  });
}

function retirerPoint(numJoueur) {
  let url = "./functions.php?idf=5&id=" + numJoueur;
  ajaxGet(url, function(reponse) {
    if (reponse == 404) {
      console.log("Il y a un souci");
    } else {
      let joueur = JSON.parse(reponse);
      let identificateur = "pointsPositifsfJoueur" + joueur.id;
      document.getElementById(identificateur).innerHTML = joueur.points;
      let idenArgent = "argentJoueur" + joueur.id;
      document.getElementById(idenArgent).innerHTML = joueur.argent;
    }
  });
}

function epargner(numJoueur, montant){
  let url = "./functions.php?idf=6&id="+numJoueur+"&montant="+montant;
  ajaxGet(url, function(reponse){
    if (reponse == 404){
      console.log("Il y a un souci");
    } else {
      let joueur = JSON.parse(reponse);
      let identificateur = "epargneJoueur"+numJoueur;
      document.getElementById(identificateur).innerHTML = joueur.epargne;
      let idArgent = "argentJoueur" + joueur.id;
      document.getElementById(idArgent).innerHTML = joueur.argent;
    }
  });
}

function retirerEpargne(numJoueur, montant){
  let url = "./functions.php?idf=7&id="+numJoueur+"&montant="+montant;
  ajaxGet(url, function(reponse){
    if (reponse == 404){
      console.log("Il y a un souci");
    } else {
      let joueur = JSON.parse(reponse);
      let identificateur = "epargneJoueur"+numJoueur;
      document.getElementById(identificateur).innerHTML = joueur.epargne;
      let idArgent = "argentJoueur" + joueur.id;
      document.getElementById(idArgent).innerHTML = joueur.argent;
    }
  });
}

function boursePlus(taux){
  let url = "./functions.php?idf=8&taux="+taux;
  ajaxGet(url, function(reponse){
    if (reponse == 404){
      console.log("Il y a un souci");
    } else {
      let joueurs = JSON.parse(reponse);
      joueurs.forEach(joueur => {
        let identificateur = "epargneJoueur"+joueur.id;
        document.getElementById(identificateur).innerHTML = joueur.epargne;
      });
    }
  });
}

function bourseMoins(taux){
  let url = "./functions.php?idf=9&taux="+taux;
  ajaxGet(url, function(reponse){
    if (reponse == 404){
      console.log("Il y a un souci");
    } else {
      let joueurs = JSON.parse(reponse);
      joueurs.forEach(joueur => {
        let identificateur = "epargneJoueur"+joueur.id;
        document.getElementById(identificateur).innerHTML = joueur.epargne;
      });
    }
  });
}

function transactionJoueur(idJoueur, montant, credit){
  let url;
  if (credit == "credit"){
    url = "./functions.php?idf=11&id="+idJoueur+"&montant="+montant+"&credit=credit";
  } else if (credit == "debit") {
    url = "./functions.php?idf=11&id="+idJoueur+"&montant="+montant+"&credit=debit";
  }
   
  ajaxGet(url, function(reponse){
      let joueur = JSON.parse(reponse);
      document.getElementById("argentJoueur"+joueur.id).innerHTML = joueur.argent;
    });
}


function resetPartie() {
  let url = "./functions.php?idf=4";
  ajaxGet(url, function(reponse) {
    if (reponse == 404) {
      console.log("Il y a un souci");
    } else {
      let informations = JSON.parse(reponse);
      informations.forEach(joueur => {
        let identificateur = "pointsPositifsfJoueur" + joueur.id;
        document.getElementById(identificateur).innerHTML = joueur.points;
        let idArgent = "argentJoueur" + joueur.id;
        document.getElementById(idArgent).innerHTML = joueur.argent;
        let idEpargne = "epargneJoueur" + joueur.id;
        document.getElementById(idEpargne).innerHTML = joueur.epargne;
      });
    }
  });
}

function echangeJoueur(joueurCredit, joueurDebit, montant){
  let url = "./functions.php?idf=10&idd="+joueurCredit+"&idc="+joueurDebit+"&montant="+montant;
  ajaxGet(url, function(reponse){
    if (reponse == 404){
      console.log("Il y a un soucis")
    } else {
      let joueurs = JSON.parse(reponse);
      joueurs.forEach(joueur => {
        let idArgent = "argentJoueur"+joueur.id;
        document.getElementById(idArgent).innerHTML = joueur.argent;
      });
    }
  })
}

document.getElementById("pointPN1").addEventListener("click", function() {
  let identificateur = this.id;
  identificateur = identificateur.split("pointPN");
  identificateur = identificateur[1];
  retirerPoint(identificateur, true);
});
document.getElementById("pointPJ1").addEventListener("click", function() {
  let identificateur = this.id;
  identificateur = identificateur.split("pointPJ");
  identificateur = identificateur[1];
  addPoint(identificateur, true);
});
// Configuration Joueur 2
document.getElementById("pointPN2").addEventListener("click", function() {
  let identificateur = this.id;
  identificateur = identificateur.split("pointPN");
  identificateur = identificateur[1];
  retirerPoint(identificateur, true);
});
document.getElementById("pointPJ2").addEventListener("click", function() {
  let identificateur = this.id;
  identificateur = identificateur.split("pointPJ");
  identificateur = identificateur[1];
  addPoint(identificateur, true);
});

// Configuration Joueur 3
document.getElementById("pointPN3").addEventListener("click", function() {
  let identificateur = this.id;
  identificateur = identificateur.split("pointPN");
  identificateur = identificateur[1];
  retirerPoint(identificateur, true);
});
document.getElementById("pointPJ3").addEventListener("click", function() {
  let identificateur = this.id;
  identificateur = identificateur.split("pointPJ");
  identificateur = identificateur[1];
  addPoint(identificateur, true);
});

// Configuration Joueur 4
document.getElementById("pointPN4").addEventListener("click", function() {
  let identificateur = this.id;
  identificateur = identificateur.split("pointPN");
  identificateur = identificateur[1];
  retirerPoint(identificateur, true);
});
document.getElementById("pointPJ4").addEventListener("click", function() {
  let identificateur = this.id;
  identificateur = identificateur.split("pointPJ");
  identificateur = identificateur[1];
  addPoint(identificateur, true);
});
document.getElementById("btnReset").addEventListener("click", function() {
  resetPartie();
});

  document.getElementById("btnEpargne").addEventListener("click", function(){

    epargner(document.getElementById("idJoueurEpargne").value, document.getElementById("montantEpargne").value);
    console.log(document.getElementById("montantEpargne").value);
    console.log(document.getElementById("idJoueurEpargne").value);
  });
  
  document.getElementById("btnRetraitEpargne").addEventListener("click", function(){
    retirerEpargne(document.getElementById("idJoueurEpargne").value, document.getElementById("montantEpargne").value);
    console.log(document.getElementById("montantEpargne").value);
    console.log(document.getElementById("idJoueurEpargne").value);
  });


  document.getElementById("btnBoursePlus").addEventListener("click", function(){
    let bourseValue = document.getElementById("bourseValue").value.replace(",", ".");
    boursePlus(bourseValue);
  });
  
  document.getElementById("btnBourseMoins").addEventListener("click", function(){
    let bourseValue = document.getElementById("bourseValue").value.replace(",", ".");
    bourseMoins(bourseValue);
  });

  document.getElementById("btnEchange").addEventListener("click", function(){
    let idJoueurCredit = document.getElementById("idJoueurCredit").value;
    let idJoueurDebit = document.getElementById("idJoueurDebit").value;
    let montant = document.getElementById("montantEchange").value;
    
    echangeJoueur(idJoueurCredit, idJoueurDebit, montant);
  });

  document.getElementById("btnTransaction").addEventListener("click", function(){
    let idJoueur = document.getElementById("idJoueurTransaction").value;
    let montant = document.getElementById("montantTransaction").value;

    transactionJoueur(idJoueur, montant, "credit");
  });
  document.getElementById("btnDebit").addEventListener("click", function(){
    let idJoueur = document.getElementById("idJoueurTransaction").value;
    let montant = document.getElementById("montantTransaction").value;

    transactionJoueur(idJoueur, montant, "debit");
  });
  // Sert à la synchro à chq refresh
  getJoueurs();

