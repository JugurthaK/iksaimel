<?php

    class DB{

        private $pdo;
        private $host;
        private $bdd;
        private $user;
        private $pw;

        public function __construct(){
            if (getenv('SERVER_ADDR') == '82.165.80.95'){
                require './config/param.php';
                $this -> host = $parameters['host'];
                $this -> bdd = $parameters['bdd'];
                $this -> user = $parameters['user'];
                $this -> pw = $parameters['pw'];
            } else {
                require ('./config/param.local.php');
                $this -> host = $parameters['host'];
                $this -> bdd = $parameters['bdd'];
                $this -> user = $parameters['user'];
                $this -> pw = $parameters['pw'];

            }
        }

        public function getPDO(){
            return new PDO("mysql:host=$this->host;dbname=$this->bdd;charset=utf8", $this->user, $this->pw);
        }


        public function getJoueurs(){
            $r = $this->getPDO() -> prepare("SELECT * FROM joueur");
            $r -> execute();
            echo (json_encode($r -> fetchAll(PDO::FETCH_ASSOC)));
        }

        public function getJoueur($id){
            $r = $this->getPDO() -> prepare("SELECT * FROM joueur WHERE id = :id");
            $r -> bindParam(':id', $id);
            $r -> execute();
            echo (json_encode($r -> fetch()));
        }   
    
        public function addPoint($id_joueur){
            
            $r = $this->getPDO() -> prepare("UPDATE joueur SET points = points + 1, argent = argent + 200 WHERE id = :id");
            $r -> bindParam(":id", $id_joueur);
            $r -> execute();
            
            $this->getJoueur($id_joueur);
        }
        
        public function retirerPoint($id_joueur){

            $r = $this->getPDO() -> prepare("UPDATE joueur SET points = points - 1, argent = argent - 200 WHERE id = :id");
            $r -> bindParam(":id", $id_joueur);
            $r -> execute();
            
            $this->getJoueur($id_joueur);
        }

        public function epargner($id_joueur, $montant){
            $r = $this->getPDO() -> prepare("UPDATE joueur SET argent = argent - :montant, epargne = epargne + :montant WHERE id = :id");
            $r -> bindParam(":id", $id_joueur);
            $r -> bindParam(":montant", $montant);
            $r -> bindParam(":montant", $montant);
            $r -> execute();

            $this -> getJoueur($id_joueur);
        }
        public function retirerEpargne($id_joueur, $montant){
            $r = $this->getPDO() -> prepare("UPDATE joueur SET argent = argent + :montant, epargne = epargne - :montant WHERE id = :id");
            $r -> bindParam(":id", $id_joueur);
            $r -> bindParam(":montant", $montant);
            $r -> bindParam(":montant", $montant);
            $r -> execute();

            $this -> getJoueur($id_joueur);
        }

        public function boursePlus($taux){
            $r = $this -> getPDO() -> prepare("UPDATE joueur SET epargne = epargne * (1.15 + :taux)");
            $r -> bindParam(":taux", $taux);
            $r -> execute();

            $this -> getJoueurs();
        }
        
        public function bourseMoins($taux){
            $r = $this -> getPDO() -> prepare("UPDATE joueur SET epargne = epargne * (0.85 - :taux)");
            $r -> bindParam(":taux", $taux);
            $r -> execute();

            $this -> getJoueurs();
        }

        public function echangeJoueur($id_credit, $id_debit, $montant){
            $r = $this -> getPDO() -> prepare("UPDATE joueur SET argent = argent + :montant WHERE id = :id");
            $r -> bindParam(":montant", $montant);
            $r -> bindParam(":id", $id_credit);
            $r -> execute();

            $r1 = $this -> getPDO() -> prepare("UPDATE joueur SET argent = argent - :montant WHERE id = :id");
            $r1 -> bindParam(":montant", $montant);
            $r1 -> bindParam(":id", $id_debit);
            $r1 -> execute();

            $this -> getJoueurs();
        }
        public function transactionJoueur($id_joueur, $montant, $credit){
            if ($credit === "credit"){
                $r = $this -> getPDO() ->prepare("UPDATE joueur SET argent = argent + :montant WHERE id = :id");
                $r -> bindParam(":montant", $montant);
                $r -> bindParam(":id", $id_joueur);
                $r -> execute();
            } else {
                $r = $this -> getPDO() ->prepare("UPDATE joueur SET argent = argent - :montant WHERE id = :id");
                $r -> bindParam(":montant", $montant);
                $r -> bindParam(":id", $id_joueur);
                $r -> execute();
            }
            $this->getJoueur($id_joueur);
        }
        public function resetPartie(){
            $r = $this->getPDO() -> prepare("UPDATE joueur SET points = 0, argent = 200, epargne = 0");
            $r -> execute();

            $r = $this->getPDO() -> prepare("SELECT * FROM joueur");
            $r -> execute();
            echo (json_encode($r -> fetchAll(PDO::FETCH_ASSOC)));
        }

        private function tableauJoueurs(){
            $joueurs = json_decode($this -> getJoueurs(), true);
            echo '<table><thead><tr>';
            echo '<th>Points</th>';
            echo '<th>Argent</th>';
            echo '<th>Epargne</th>';
            echo '</tr></thead><tbody>';

            foreach ($joueurs as $joueur) {
                echo '<tr>';
                echo '<td>' .$joueur['points'].'</td>';
                echo '<td>' .$joueur['argent'].'</td>';
                echo '<td>' .$joueur['epargne'].'</td>';
                echo '</tr>';
            }
            echo '</tbody></table>';
        }
    }


?>