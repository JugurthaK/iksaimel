<?php
    header('Content-Type: application/json');
    require './class/DB.php';
    $db = new DB();

    switch ($_GET['idf']) {
        case '1':
            $db -> getJoueurs();
            break;
        case '2':
            $db -> getJoueur($_GET['id']);
            break;
        case '3':
            $db -> addPoint($_GET['id']);
            break;
        case '4':
            $db -> resetPartie();
             break;
        case '5':
            $db -> retirerPoint($_GET['id']);
            break;
        case '6':
            $db -> epargner($_GET['id'], $_GET['montant']);
            break;
        case '7':
            $db -> retirerEpargne($_GET['id'], $_GET['montant']);
            break;
        case '8':
            $db -> boursePlus($_GET['taux']);
        break;
        case '9':
            $db -> bourseMoins($_GET['taux']);
        break;
        case '10':
            $db -> echangeJoueur($_GET['idc'], $_GET['idd'], $_GET['montant']);
        break;
        case '11':
            $db -> transactionJoueur($_GET['id'], $_GET['montant'], $_GET['credit']);
        default:
            # code...
            break;
    }

?>